import { Component } from '@angular/core';
import { HistoryService } from '../services/history.service';
import { HistoryItem } from '../services/models.interface';

@Component({
    selector: 'home',
    styleUrls: ['./home.component.scss'],
    template: `
        <div class="wrapper">
            <span>LAST LUCKY PERSONS</span>
            <hr class="sep" />
            <list-item *ngFor="let history of histories" [item]="history">
            </list-item>
        </div>
    `
})
export class HomeComponent {
    histories: HistoryItem[];
    constructor(private historyService: HistoryService) {
        // this.histories = historyService.getHistories();
        this.historyService.getHistories()
            .subscribe((data: HistoryItem[]) => {
                this.histories = data;
            });
    }


}