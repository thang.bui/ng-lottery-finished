import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { BetComponent } from './bet/bet.component';
import { PlacingBetComponent } from './bet/placing-bet.component';
import { ListItemComponent } from './list-item/list-item.component';

import { HistoryService } from './services/history.service';
import { GameService } from './services/game.service';

const routes: Routes = [
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'bet', component: BetComponent
  },
  {
    path: 'placingBet', component: PlacingBetComponent
  },
  {
    path: '**', redirectTo: '/home', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    BetComponent,
    PlacingBetComponent,
    ListItemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    HistoryService,
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
