import { Component, Input } from '@angular/core';
import { HistoryItem } from '../services/models.interface';

@Component({
    selector: 'list-item',
    styleUrls: ['./list-item.component.scss'],
    template: `
        <div class="list-item">
            <span class="player-name">{{item.player}}</span>
            <ul class="bet-number">
                <li *ngFor="let betNumber of item.betSequences">{{betNumber}}</li>
            </ul>
            <span class="date">{{item.date}}</span>
        </div>
    `
})
export class ListItemComponent {
    @Input() item: HistoryItem;
}