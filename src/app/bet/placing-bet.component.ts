import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../services/game.service';
import { Bet } from '../services/models.interface';

@Component({
    selector: 'placing-bet',
    styleUrls: ['./placing-bet.component.scss'],
    templateUrl: './placing-bet.component.html'
})
export class PlacingBetComponent {
    player = '';
    betSequences = [null, null, null, null, null, null];

    constructor(private router: Router, private gameService: GameService) {}

    cancel() {
        this.router.navigate(['/bet']);
    }

    placeBet() {
        const newBet = {
            player: this.player,
            betSequences: this.betSequences
                .map((item: number) => (item.toString().padStart(2, '0')))
        } as Bet;
        if (this.gameService.bets.findIndex(item => item.player === newBet.player) !== -1) {
            alert('duplication player');
            return;
        }

        this.gameService.placeBet(newBet);
        this.router.navigate(['/bet']);
    }
}