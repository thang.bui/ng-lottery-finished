import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../services/game.service';
import { Bet, BetResult } from '../services/models.interface';

@Component({
    selector: 'bet',
    styleUrls: ['./bet.component.scss'],
    template: `
        <div class="wrapper">
            <div class="mask" *ngIf="isWaiting">
                <div class="loader">Waiting...</div>
            </div>
            <div *ngIf="!isWaiting && !showResult">
                <span>CURRENT BETS</span>
                <hr class="sep" />
                <div class="actions">
                    <button class="btn btn-link" (click)="placeBet()">Place a bet</button>
                    <button class="btn btn-link" (click)="startGame()">Start</button>
                </div>
                <list-item *ngFor="let bet of bets" [item]="bet">
                </list-item>
                <!--
                <list-item [item]="testBet">
                </list-item>-->
            </div>
            <div *ngIf="showResult">
                <span>RESULT</span>
                <hr class="sep" />
                <list-item class="result" [item]="{ betSequences: betResult.resultSequences }"></list-item>
                <span>WINNERS</span>
                <hr class="sep" />
                <list-item *ngFor="let winner of betResult.winners" [item]="winner">
                </list-item>
            </div>
        </div>        
    `
})
export class BetComponent {
    bets: Bet[];
    betResult: BetResult;
    testBet: Bet = {
        player: 'test', betSequences: ['01', '02']
    };
    isWaiting: boolean;
    showResult: boolean;
    constructor(private gameService: GameService, private router: Router) {
        this.bets = this.gameService.bets;
    }

    placeBet() {
        this.router.navigate(['/placingBet']);
    }

    startGame() {
        this.isWaiting = true;
        this.gameService.play()
            .subscribe((result: BetResult) => {
                this.isWaiting = false;
                this.betResult = result;
                this.showResult = true;
            });
    }
}