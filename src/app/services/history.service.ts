import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HistoryItem } from './models.interface';

@Injectable()
export class HistoryService {
    private _histories: HistoryItem[];
    constructor(private http: HttpClient) {
        this.initDefaultHistories();
    }

    getHistories(): Observable<HistoryItem[]> {
        return Observable.of(this._histories);
    }

    // getHistories(): Observable<HistoryItem[]> {
    //     return this.http.get('./assets/history.json')
    //         .map((result: any) => {
    //             console.log('http response', result);
    //             return result.data;
    //         });
    // }

    add(player: string, sequence: string[]) {
        this._histories = [
            { player: player, betSequences: sequence, date: new Date().toDateString() },
            ...this._histories
        ];
    }

    private initDefaultHistories() {
        this._histories = [
            { player: 'John Snow', date: '2017-10-26',
                betSequences: ['01', '02', '03', '04', '05', '06'] } as HistoryItem,
            
            { player: 'Thang Bui', date: '2017-09-26',
                betSequences: ['11', '12', '13', '14', '15', '16'] } as HistoryItem,
        ];
    }

}
