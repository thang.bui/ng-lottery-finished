import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { Bet, BetResult } from '../services/models.interface';
import { HistoryService } from './history.service';

@Injectable()
export class GameService {
    private _bets: Bet[];
    constructor(private historyService: HistoryService) {
        this._bets = <Bet[]>[];
    }

    get bets(): Bet[] {
        return this._bets;
    }

    placeBet(newBet: Bet) {
        this._bets.push(newBet);
    }

    reset() {
        this._bets = <Bet[]>[];
    }

    play(): Observable<BetResult> {
        const sequence = this.randomResult();
        
        const winners = this.calculateWinner(sequence);
        const betResult = {
            winners: winners,
            resultSequences: sequence
        } as BetResult;

        // writing history
        if (winners.length > 0) {
            winners.forEach((item: Bet) => {
                this.historyService.add(item.player, item.betSequences);
            });
        }

        this.reset();
        return Observable.of(betResult).delay(5000);
    }

    private randomResult(): string[] {
        const sequence: string[] = [];
        
        for(let i = 0; i < 6; i++) {
            let randomNumber = this.randomInRange(0, 45);
            while (sequence.includes(randomNumber.toString())) {
                randomNumber = this.randomInRange(0, 45);
            }

            sequence.push(randomNumber);
        }
        return sequence;
    }

    private randomInRange(min: number, max: number): string {
        min = Math.ceil(min);
        max = Math.floor(max);
        const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;

        return randomNumber.toString().padStart(2, '0');

    }

    private calculateWinner(result: string[]): Bet[] {
        let winner: Bet[] = [];
        // at least one couple numbers has to be matched
        let highestScore = 1;
        this._bets.forEach((bet: Bet) => {
            let matchedTimes = 0;
            result.forEach(item => {
                if (bet.betSequences.indexOf(item) !== -1) {
                    matchedTimes++;
                }
            });
            
            if (matchedTimes > highestScore) {
                winner = [bet];
                highestScore = matchedTimes;
            } else if (matchedTimes === highestScore) {
                winner.push(bet);
            }
        });

        return winner;
    }
}