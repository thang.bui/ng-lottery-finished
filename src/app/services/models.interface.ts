export class Bet {
    player: string;
    betSequences: string[];
}

export class HistoryItem extends Bet {
    date: string;
}

export interface BetResult {
    winners: Bet[];
    resultSequences: string[];
}
